package com.afzal;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/CalculatorServlet")
public class CalculatorServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        // Getting parameters from the HTML form
        int num1 = Integer.parseInt(request.getParameter("num1"));
        int num2 = Integer.parseInt(request.getParameter("num2"));
        String operator = request.getParameter("operator");

        // Performing calculation
        double result = 0;
        switch (operator) {
            case "+":
                result = num1 + num2;
                break;
            case "-":
                result = num1 - num2;
                break;
            case "*":
                result = num1 * num2;
                break;
            case "/":
                if (num2 != 0) {
                    result = (double) num1 / num2;
                } else {
                    out.println("Cannot divide by zero!");
                    return;
                }
                break;
            default:
                out.println("Invalid operator");
                return;
        }

        // Displaying result in HTML
//        out.println("<html><head><title>Result</title></head><body>");
//        out.println("<h2>Result:</h2>");
//        out.println("<p>" + num1 + " " + operator + " " + num2 + " = " + result + "</p>");
//        out.println("</body></html>");

//        send result back to html
        response.getWriter().write(Double.toString(result));
    }	
}
