function calculate() {
        var num1 = document.getElementById("num1").value;
        var num2 = document.getElementById("num2").value;
        var operator = document.getElementById("operator").value;

        fetch("CalculatorServlet", {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            body: "num1=" + encodeURIComponent(num1) + "&num2=" + encodeURIComponent(num2) + "&operator=" + encodeURIComponent(operator)
        })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.text();
        })
        .then(result => {
            document.getElementById("result").innerHTML = "Result: " + result;
        })
        .catch(error => {
            console.error("Error:", error);
            document.getElementById("result").innerHTML = "Error occurred while processing the request.";
        });
    }