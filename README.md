# Calculator Servlet Application

This is a simple calculator application built using Java Servlets with the help of Eclipse IDE.

## Table of Contents

- [Features](#features)
- [Prerequisites](#prerequisites)
- [Getting Started](#getting-started)
- [Usage](#usage)

## Features

- Addition: Perform addition of two numbers.
- Subtraction: Perform subtraction of two numbers.
- Multiplication: Perform multiplication of two numbers.
- Division: Perform division of two numbers.

## Prerequisites

- Java Development Kit (JDK) installed
- Eclipse IDE installed

## Getting Started

1. Clone the repository:

    ```bash
    git clone https://gitlab.com/Afzal-dev2/calculatorservletapplication
    ```

2. Open the project in Eclipse IDE.

3. Run the project.

4. Select your Apache Tomcat server. This will deploy the application in localhost.

5. Access the application in your web browser:

    ```
    http://localhost:8080/CalculatorApplication/index.html
    ```

## Usage

1. Enter two numbers in the input fields.

2. Select the desired operation from the dropdown menu.

3. Click on the "Calculate" button.

4. The result will be displayed on the page.

## Screenshots

Here are some screenshots of the Calculator Servlet Application:

![Screenshot 1](/screenshots/screenshot1.png)
![Screenshot 2](/screenshots/screenshot2.png)